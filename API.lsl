/*
    Nirahiel's ATLust Library
    Meant to be used with the ATLust API.

    Read comments, they're worth it !

    Made and owned by Nirahiel (nirahiel resident)
    Use at your own risk.
*/

#include "ATLust_API/key.lsl"

#define atnet_channel       "atnet" //The server channel that we are working with. (WARNING: All scripts must be on the same channel, otherwise they will not talk to each other)
#define api_version         "2.0"   //Do not change this. Doing so may break the script.

#define ATLUST_SIZE_CHECK

//Constants ----------------

/*
    Note: The constant names and ID's will stay the same, but the parameters that are sent and received may change.
    Please use the wiki as the official source: https://atlust.fandom.com/wiki/V2_API
*/

//Commands ------------
//Level 1 API (Public)
#define atl_api_test                    10001    //Test request.
#define atl_api_get_stats               10002    //Returns lust, arousal, sensitivity, health and sex status. (lust, arous, sens, health, sex)
#define atl_api_get_partner_arousal     10003    //Returns the arousal level of the specified partner. Returns -1.0 if not having sex with the target. (Requires: target) (Response: target, arousal)
#define atl_api_get_all_organs          10004    //Returns an object with all organ types and the ID's of each individual organ.
#define atl_api_get_organ_info          10005    //Returns an object with organ info (Requires: organ_type, index)
#define atl_api_get_sex_actions         10006    //Returns a list of objects with sex action info (Returns: [{init, other_player, id, name, pen, rec_organ, init_organ}])
#define atl_api_get_is_atlust           10007    //Returns true or false if the target has ATLust. (Requires: target)
#define atl_api_get_nearby_players      10008    //Returns a list of UUID's of all nearby ATLust players.
#define atl_api_get_conditions          10009    //Returns a list of condition names
#define atl_api_get_attraction_to       10010    //Returns the attraction amount to the target character/player. Returns NOT_FOUND if the target is not found. (Requires: char_id or player_id) (Returns: target, attraction)
#define atl_api_get_gender              10011    //Returns the gender info of the target character/player. (Returns: ident, bal, penis, vag) (Requires: char_id or player_id)
#define atl_api_get_name                10012    //Returns the target character's name and title. Returns NOT_FOUND if the target is not found (Requires: char_id or player_id) (Returns: name, title)
#define atl_api_get_char_id             10013    //Returns the players active character ID. (Requires: player_id) (Returns: char_id, char_name)
#define atl_api_get_char_species        10014    //Returns the characters species. (Requires: player_id or char_id) (Returns: target, species)
#define atl_api_get_preg_count			10015	 //Returns a key-value list of the number of pregnancies in each organ.	
#define atl_api_get_breeding_ground     10016    //Returns the data of the breeding ground the player is currently on. (Returns: name, desc, active, players)
#define atl_api_get_pregnancy_progress	10017	 //Returns a floating point value (0.0-1.0) of the highest pregnancy progress.
#define atl_api_get_dtf_status          10018    //Returns a key-value pair of the various DTF statuses (dtf, auto_partner, auto_rp, sex_doll)
#define atl_api_get_connected_portals   10019   //Returns a list of all connected portals (The portal_id)
#define atl_api_get_organ_by_type       10020   //Returns a list of all organ ID's for the specified type of organ. (Requires: organ_type)
//Level 2 API (API Key required)
#define atl_api_register_notifications  20000    //Registers the object to receive API notifications. Returns "OK" on success.
#define atl_api_change_char             20001    //Changes to the indicated character. (Requires: char_id)
#define atl_api_set_arousal             20002    //Changes the active characters arousal to the indicated value. (Requires: arous)
#define atl_api_set_lust                20003    //Changes the active characters lust ot the indicated value. (Requires: lust)
#define atl_api_get_infections          20004    //Returns a list of infection names that the active character has
#define atl_api_get_desires             20005    //Returns a list of desire names that the active character has
#define atl_api_get_cum_list			20006	//Returns a list of all cum in the player's current character. (Returns: sources, vol, known, anon, pot)
#define atl_api_get_relations			20007	//Returns a list of related characters to the player's current character. (Returns: char_id, relation_amount)
#define atl_api_get_pregnancy			20008	//Returns the pregnancies in the target organ. (Requires: organ_type, organ_index) (Returns: species, due_time, father_char, father_anon, mother_char)
#define atl_api_mod_arousal             20009   //Changes the active characters arousal by the amount specified (Requires: arous)
#define atl_api_mod_lust                20010   //Changes the active characters lust by the amount specified (Requires: lust)
#define atl_api_connect_portal          20011   //Connects the indicated organ to the portal network. Returns the portal ID (Requires: organ_type, index, or organ_id)
#define atl_api_disconnect_portal       20012   //Disconnects the indicated organ from the portal network. Returns "OK" (Requires: organ_type, index, or organ_id)
#define atl_api_begin_portal_sex        20013   //Begins portal sex with the indicated portal connection. Returns "OK" (Requires: portal_id and organ_type, index, or organ_id)
#define atl_api_disable_organ           20014   //Disables the indicated organ for 5 minutes.(Requires: organ_type, index, or organ_id)
//Commands ------------

//Signals (API Notifications) ------------
#define atl_api_signal_stats                    1000    //Stats update, automatically sent out every minute. (lust, arousal, sensitivity, health)    
#define atl_api_signal_sex_start                1001    //A sex action has started (action_id, action_name, init_char, rec_char, init_agent, rec_agent, rec_species, init_species)
#define atl_api_signal_sex_stop                 1002    //A sex action has stopped (action_id)
#define atl_api_signal_sex_stop_all             1003    //All sex actions stopped.
#define atl_api_signal_play_action_received     1004    //Another character played with your character (action_id, action_name, init_char, rec_char, init_agent, rec_agent)
#define atl_api_signal_play_action_given        1005    //Character played with another character (action_id, action_name, init_char, rec_char, init_agent, rec_agent)
#define atl_api_signal_climax                   1006    //Character climaxed (organ, other_organ, other_player, penetrative, rp_mode)
#define atl_api_signal_partner_climaxed         1007    //Partner's character climaxed (organ, other_organ, other_player, penetrative, rp_mode)
#define atl_api_signal_tease_action_given       1008    //Character teased another character  (action_id, action_name, init_char, rec_char, init_agent, rec_agent)
#define atl_api_signal_tease_action_received    1009    //Another character teased your character  (action_id, action_name, init_char, rec_char, init_agent, rec_agent)
#define atl_api_signal_preclimax_response       1010    //The player responded to the preclimax question. (response)
#define atl_api_signal_char_data_updated        1011    //The player changed their character data (type)
#define atl_api_signal_char_changed             1012    //The player changed their active character. (old, new)
#define atl_api_signal_item_used                1013    //The player used an item (item)
#define atl_api_signal_condition_gained         1014    //The character gained a new condition (name, mod)
#define atl_api_signal_condition_removed        1015    //The character lost a condition (name, mod)
#define atl_api_signal_titled_earned            1016    //The character earned a new title (name)
#define atl_api_signal_inseminated              1017    //The character has been inseminated (organ, species, amount, known, anonymous, rp_mode, partner_id)
#define atl_api_signal_impregnated              1018    //The character has been impregnated (species, due_time, father_char, father_anon, mother_char)
#define atl_api_signal_ovulated                 1019    //The characters womb has released an ovum (expire)
#define atl_api_signal_gave_cum                 1020    //The character inseminated another character (organ, amount, known, anonymous, rp_mode, partner_id)
#define atl_api_signal_received_eggs            1021    //The character has been oviposited into (organ, fertilized, source_char, source_player, rp_mode)
#define atl_api_signal_cum_on_body              1022    //The character has received cum on their body (organ, amount, rp_mode)
#define atl_api_signal_knot_tied                1023    //The character has been knotted, or knotted someone else. When the penetrative sex action stops is when the knot is released. (knot_giver, knot_receiver)
#define atl_api_signal_api_access_response  	1024	//The player responded to an API permissions request. (id, accept)	
#define atl_api_venom_received                  1025    //The character has been injected with venom
#define atl_api_venom_given                     1026    //The character has injected another with venom
#define atl_api_gave_birth			            1027    //The character has given birth or laid an egg.
#define atl_api_portal_connected                1028    //The character connected an organ to the portal network (organ_id, organ_type)
#define atl_api_portal_sex_began                1029    //Portal sex has began (organ_id, organ_type, other_organ_type)
#define atl_api_portal_sex_ended                1030    //Portal sex has ended (organ_id, organ_type, other_organ_type)
#define atl_api_portal_disconnected             1031    //The character disconnected an organ from the portal network (organ_id, organ_type)
#define atl_api_portal_sex_progress             1032    //Portal sex is in progress (organ_id, organ_type, other_organ_type, arousal)

#define atl_api_err_invalid_api_id              -1      //The API script provided an invalid API ID (Non-integer or ID doesn't exist)
#define atl_api_err_no_perms                    -2      //The player hasn't given permission or has disabled permissions for the API.
#define atl_api_err_checksum_mismatch           -3      //The hash failed verification
#define atl_api_err_internal_error              -4      //The server encountered and error while processing the request.
//Signals (API Notifications) ------------

// For a size 2 strided list of keys and values, get the corresponding value for a given key
#define listItemByKey(myList, Key, type) ((type)myList[llListFindList(myList, (list)Key) + 1])

integer atl_currentRequestId = 0;
integer ATRequestChannel = 0;


integer ATLust_GetNextRequestId()
{
    atl_currentRequestId++;
    return atl_currentRequestId;
}

integer ATLust_GetListenChannel()
{
    return llHash((string)llGetOwner() + "(C) Aozora Tech 2021 - ATLust API" + atnet_channel);
}

string ATLust_GenerateRequest(integer command_id, list extra, integer request_id)
{
    return llList2Json(JSON_OBJECT, ["cmd", command_id,
                                     "id", request_id,
                                     "extra", llList2Json(JSON_OBJECT, extra)]);
}

ATLust_SendCommands(list requests)
{   
    string body = llList2Json(JSON_ARRAY, requests);
    
    //If using level 2 or higher API's, a cryptographic hash is required. You will receive the hash function once you receive higher level permissions.
    string hash = "";//Only to be used with level 1/public API commands.

    if (api_id != "0") {
        hash = ATLust_Hash(body);
    }
    
    string msg = api_id + "/" + hash + "/" + api_version + "/" + body;
    
    #ifdef ATLUST_SIZE_CHECK
    //OPTIONAL. Checks to make sure the message isn't too long.
    if(llStringLength(body) > 2040)//If our message is longer than 2048 characters, it will get truncated when sent. If this happens, it will fail to be processed.
    {
        llOwnerSay("WARNING: The last request sent may be too long!");
    }
    #endif
    llRegionSayTo(llGetOwner(), ATLust_GetListenChannel(), msg);
}

integer ATLust_HandleHUDMessage(integer channel, string name, key id, string message) {
    if(llGetOwnerKey(id) != llGetOwner())
    {
        return FALSE;
    }
    integer call_id = (integer)llJsonGetValue(message, ["call_id"]);
    if (call_id == atl_api_signal_condition_gained) // string name, string mod
    {
        #ifdef atl_api_signal_condition_gained_callback
        list signal = llJson2List(llJsonGetValue(message, ["extra"]));
        atl_api_signal_condition_gained_callback(
            listItemByKey(signal, "name", string),
            listItemByKey(signal, "mod", string)
        );
        #endif
        return TRUE;
    }
    if (call_id == atl_api_signal_condition_removed) // string name, string mod
    {
        #ifdef atl_api_signal_condition_removed_callback
        list signal = llJson2List(llJsonGetValue(message, ["extra"]));
        atl_api_signal_condition_removed_callback(
            listItemByKey(signal, "name", string),
            listItemByKey(signal, "mod", string)
        );
        #endif
        return TRUE;
    }
    if (call_id == atl_api_signal_api_access_response)// No arguments
    {
        string _api_id = llJsonGetValue(message, ["extra", "api_id"]);
        if (_api_id != api_id) return FALSE;
        if (llJsonValueType(message, ["extra", "accepted"]) == JSON_TRUE)
        {
            #ifdef atl_api_permission_granted
                atl_api_permission_granted();
            #endif
        }
        else
        {
            #ifdef atl_api_permission_denied
                atl_api_permission_denied();
            #endif
        }
        llSetTimerEvent(0);
        return TRUE;
    }
    if (call_id == atl_api_signal_stats) // float lust, float arousal, float sensitivity, float health
    {
        #ifdef atl_api_signal_stats_callback
        list signal = llJson2List(llJsonGetValue(message, []));
        atl_api_signal_stats_callback(
            listItemByKey(signal, "lust", float),
            listItemByKey(signal, "arousal", float),
            listItemByKey(signal, "sensitivity", float),
            listItemByKey(signal, "health", float)
        );
        #endif
        return TRUE;
    }
    if (call_id == atl_api_signal_play_action_received)// integer action_id, integer init_char, integer rec_char, string action_name, key init_agent, key rec_agent, float rec_arousal
    {
        #ifdef atl_api_signal_play_action_received_callback
        list signal = llJson2List(llJsonGetValue(message, ["extra"]));
        atl_api_signal_play_action_received_callback(
            listItemByKey(signal, "action_id", integer),
            listItemByKey(signal, "init_char", integer),
            listItemByKey(signal, "rec_char", integer),
            listItemByKey(signal, "action_name", string),
            listItemByKey(signal, "init_agent", key),
            listItemByKey(signal, "rec_agent", key),
            listItemByKey(signal, "rec_arousal", float)
        );
        #endif
        return TRUE;
    }
    if (call_id == atl_api_signal_play_action_given)// integer action_id, integer init_char, integer rec_char, string action_name, key init_agent, key rec_agent, float rec_arousal
    {
        #ifdef atl_api_signal_play_action_given_callback
        list signal = llJson2List(llJsonGetValue(message, ["extra"]));
        atl_api_signal_play_action_given_callback(
            listItemByKey(signal, "action_id", integer),
            listItemByKey(signal, "init_char", integer),
            listItemByKey(signal, "rec_char", integer),
            listItemByKey(signal, "action_name", string),
            listItemByKey(signal, "init_agent", key),
            listItemByKey(signal, "rec_agent", key),
            listItemByKey(signal, "rec_arousal", float)
        );
        #endif
        return TRUE;
    }

    return FALSE;
}


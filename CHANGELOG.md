#### V1.1.1
* Added missing critical info for the readme.

### V1.1
* Removed obsolete function
* Updated the README

## V1.0 - Initial Release
Did a first draft of the library.
More stuff to come !

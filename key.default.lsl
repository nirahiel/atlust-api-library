//This is the default file for the key. Copy this file into key.lsl and edit appropriately.

#define api_id              "0"   //Your API ID. This is used to identify your scripts to the server. Must match the API key that was provided to you. Can be left as 0 if only using the public API.
#define api_key             "" //Your API key (Required to receive signals and send requests that start with numbers above 1). Can be left blank if only using public API.

string ATLust_Hash(string body) {
    return ""; // Replace this with your private hash function. You know what it is. (Otherwise, ask Ariu)
}
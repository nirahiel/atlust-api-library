# ATLust API Library

A library written in LSL to interact with the API of the ATLust tool and service, available at https://aozoratech.net/atnet/index.jsp?page=prod_atlust

## Getting started

You'll need to use a viewer compatible with a preprocessor, such as [Firestorm](https://www.firestormviewer.org/os/).
* Setup the library in your includes directory.  
Either clone the project into it or download the project as a zip and extract it into its own folder.  
If your includes directory is named `includes`, the structure must looks like :

        includes/
        └── ATLust_API/
            ├── .gitignore
            ├── API.lsl
            ├── CHANGELOG.md
            ├── key.default.lsl
            └── README.md

* Copy the file `key.default.lsl` into `key.lsl`
* If you have access to the level 2 API, edit your newly created `key.lsl` with the correct values.

## Usage

At the top of your script, first define any function you want to use as callback for the API signals, then include the library.
For example, if you want to have a function be called every time the HUD sends the `atl_api_signal_stats` signal, you can do it like so :

    #define atl_api_signal_stats_callback handleStats
    handleStats(float lust, float arousal, float sensitivity, float health) {
        //Do something with these values
    }
    #include "ATLust_API/API.lsl"

> Note : You can __NOT__ change states inside a function defined like so.

Next, listen to the hud using :

    llListen(ATLust_GetListenChannel(), "", NULL_KEY, "");

> Note : The value of `ATLust_GetListenChannel` doesn't change, you may want to store it in a variable.

And finally, in the listen event, send the data to the library :
    
    listen(integer channel, string name, key id, string message)
    {
        if (ATLust_HandleHUDMessage(channel, name, id, message)) {
            return; //The library handled the message, no further action required.
        }
        //Here, this message wasn't handled by the library, you might want to handle it yourself.
    }

------------------------

_This Readme will be updated at a later date_
